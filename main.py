#!/usr/bin/env python3
from dataclasses import dataclass
import typer
from enum import Enum


class Gender(Enum):
    MALE = "male"
    FEMALE = "female"


app = typer.Typer()


@app.command()
def hello(name: str):
    print(f"Hello, {name}!")


@app.command()
def goodbye(name: str, gender: Gender, formal: bool = True):
    if formal:
        prefix = "Mr" if gender == Gender.MALE else "Mrs"
        print(f"Goodbye, {prefix}. {name}!")
        return
    print(f"See ya, {name}!")


if __name__ == "__main__":
    app()
