# Typer - CLI Tools Framework

Like FastAPI, typer uses metaprogramming with python type hints to validate data types in python.

# Links
- [Typer](https://typer.tiangolo.com/)